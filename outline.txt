Here I’m going to write about the versatility of CSS3 in creating effects. I don’t know what the scope of the post should be, so if I should cut back let me know.

CSS3 Eye Candy for 2014

In 2009, Google announced at the annual I/O event their backing of new fangled web technologies, like HTML5 and CSS3, and all of the pretty web goodness they will bring. And of course, others weren't shy to follow.
It’s the year 2014. What are some cool things an intrepid web designer do with all this? Well, quite a bit! 
(outline here:, probably will write code for live examples)

Do more with fewer images
	CSS rules for common design tropes (ie: rounded corners and drop shadows)
	CSS Image manipulation (filters)

Complex Animation with a few lines of code
	CSS animations and keyframes
		Animated flatness
		Animation as an effect, instead of a big production

Do more with each tag
	Psuedo-Elements
	Now, there’s more where that came from. Here are some places where you can see the what CSS3 is capable of:

	CSS Tricks by Chris Coyier
	Smashing Magazine
	Codrops Blueprints
